package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-index";

    @NotNull
    private final String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request =
                new TaskChangeStatusByIndexRequest(getToken(), index, Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
